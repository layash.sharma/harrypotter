# Harry Potter

## Technical Documentation

### Project Setup

#### Install Node and Npm

+ For downloading node visit [Download Node](https://markdownlivepreview.com/) .
+ Clone the project from the [gitlab](https://gitlab.com/layash.sharma/harrypotter).
+ Go to the project. For example: `cd projects/harrypotter`
+You will be in the master branch.
+ Run `npm install` , it will install all the prerequisite packages.
+ Run `nodemon`


##### Remarks:
+ During development:

    
|               | Versions      |
| ------------- |:-------------:|
| Node          | 12.16.1       |
| Npm           | 6.13.4        |

+  Versions can be seen on the package.json file.


##### Project Assumptions
+ No databse used but  in-memory state is used.
+ There are only four character available at the moment.


## API Documentation

### All characters [/]

#### Get All Characters [GET]

+ Response 200 (application/json)


### Details [/character/:id]

#### Details [GET]

+ Response 200 (application/json)

    + Body

        {
            "success": true,
            "message": "Details found.",
            "data": {
                "id": "sirius-black",
                "name": "Sirius Black",
                "dob": "1959-11-03",
                "birthPlace": "Islington, London",
                "nickNames": ["Padfoot","Snuffles"],
                "image": "https://i.imgur.com/xHtlsT7.jpg"
            }
        }


### Thank you.
